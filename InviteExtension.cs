using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;

namespace InviteExtension
{
    public class InviteExtension : AOPluginEntry
    {
        public override void Run(string pluginDir)
        {
            Chat.WriteLine("InviteExtension loaded\n USAGE:/invex charid  (charid can be found with SHIFT+F9)");
            
            Chat.RegisterCommand("invex", OnCommand);
        }

        private void OnCommand(string arg1, string[] arg2, ChatWindow arg3)
        {
            switch (arg1.ToUpper())
            {
                case "INVEX":

                    if (arg2.Length > 0)
                        if (int.TryParse(arg2[0], out int val))
                        {
                            Network.Send(new CharacterActionMessage()
                            {
                                Action = CharacterActionType.TeamRequest,
                                Target = new Identity(val),
                                Parameter1 = 1
                            });
                        }
                    break;
            }
        }
    }
}
